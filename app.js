"use strict"
const express = require('express')
const app = express()
const {dev,prod} = require('yargs').argv
const env = dev ? 'dev' : 'prod'
const config = require('./config')[env]
const routes = require('./routes')
const services = require('./services')
const{
    autenticationService
} = services
const cors = require('cors')


const {
    createServer
} = require('./server')

class App {
    async run(){
        createServer({
            app: this.configure(),
            config
        })

            
    }

    configure =  () => {
        app.use(cors()); 
        app.use('*',(req,res,next) => autenticationService(req,res,next))
        

        app.get('/',(req,res)=>{
            res.send('hello').end(200)
        })
        for( let i in routes ){
            let {
                method,
                handle,
                faults,
                instances
            } = routes[i]

            app[method](i, async (req,res)=>{
                try{
                    let required = {}
                    for(let i in instances){
                        services[i].prototype.config = config[i]
                        let s = new services[i]
                        required[i] = s
                    }
                    await handle(req,res,required)
                }catch(e){
                    console.log(e.message)
                    switch(e.message){
                        case 'zero-result':
                             res.status(404).send({success: 'error',error:'zero-result'})
                            break
                        default:
                            const error = faults[e.message] || 'uncaught error'
                            res.status(500).send({success: 'error',error})

                    }
                    
                }
                
            })
        }

        return app
        
    }

    
    
}

module.exports = App