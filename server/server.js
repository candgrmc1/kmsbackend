"use strict"
const http = require('http')

const createServer = ({app,config:{port}}) => {
    const server = http.createServer(app)

    // instead "process.on" to catch termination
    server.on('SIGTERM',()=>{
        console.log('terminating..')
    })

    server.listen(port)

   
}



module.exports = {
    createServer
}