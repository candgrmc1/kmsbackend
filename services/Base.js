"use strict"
const axios = require('axios');

module.exports= class  {
    get = async (url) =>{
        try{
            const {data} = await axios.get(url)
            return data
        }
        catch(e){
            throw new Error('zero-result')
        }
       
    }

    
}