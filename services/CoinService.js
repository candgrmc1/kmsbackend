"use strict"
const Base = require('./Base')

class CoinService extends Base {
    async getLatesValueOf(currency){
        return  await this.get(`${this.config.baseUrl}/assets/${currency}`)
        
    }
}
CoinService.prototype.config = null
module.exports = CoinService