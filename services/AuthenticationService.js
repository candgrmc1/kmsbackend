"use strict"

const getToken = (req) => {
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {

        return req.headers.authorization.split(' ')[1];
    } 
    else if (req.query && req.query.token) {

        return req.query.token;
    } 
   
    return null;
}

const authenticate = (req, res, next) => {
    console.log('validation')
    if(getToken(req) !== 'test-token') {
        res.status(401).send('authentication error')
    }
    next()
}

    

module.exports = authenticate