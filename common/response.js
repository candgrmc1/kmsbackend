"use strict"

const convertParams = (data,params)=>{
    let response = {}
    for(let param in params){
        if(!data[param]){
            console.log(data)

            throw new Error('response-parameters-error')
        }
        response[params[param]] = data[param]
    }
    return response
}

const result = (r,params) =>{
    
    let response = r ? convertParams(r,params) : []
    return {
        success:'ok',
        response
    }

    
}


module.exports = result