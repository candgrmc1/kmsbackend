"use strict"
const appInstance = require('./app')
const app = new appInstance()

app.run().then(()=>{
    console.log('app is running')
}).catch(e => console.error(e))