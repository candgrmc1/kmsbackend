"use strict"
const instances = {
    coinService: null
}

const {response} = require('../common') 

module.exports = {
    "/coin" : {
        method: 'get',
        faults: {
            'external-error': 'Hata',
            'response-parameters-error': 'Hatalı response||parametre',
            'empty-response': 'Boş Response'
        },
        instances,
        handle: async (req,res, services) => {
            const {
                coinService
            } = services
            const {
                data
            } = await coinService.getLatesValueOf(req.query.search)

            
            res.json(response(data,{
                symbol: 'symbol',
                priceUsd: 'price'
            })).end(200)
        }
    }
}